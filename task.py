import boto3 as boto3
import whatismyip


def entry_point(cldr):
    # this builds a lazy task graph and does not execute the function
    results = cldr.client.map(print_ip, range(10))

    # this executes the graph
    outcome = cldr.client.gather(results)

    print(outcome)


def entry_point_boto_test(cldr):
    results = cldr.client.map(test_to_s3, range(3))

    outcome = cldr.client.gather(results)

    print(outcome)


def test_to_s3(x):
    bucket = "airflow-crossref-research-annotation"
    path = f"distrunner-touch{x}.txt"
    session = boto3.Session()
    s3 = session.resource("s3")
    obj = s3.Object(bucket, path)
    obj.put(Body="Test content")


def print_ip(x):
    return f"My IP address is {whatismyip.whatismyip()}"
